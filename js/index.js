import { pointInSegment } from "./utils.js";
import jseEval from 'https://cdn.jsdelivr.net/npm/jse-eval@1.5.2/+esm'



if (typeof AFRAME === 'undefined') {
    throw new Error('Component attempted to register before AFRAME was available.');
}


//////////
// GLOBALS

// Times
export var MAX_TIME_UNIT = 100
export var MIN_TIME_UNIT = 1
export var INITIAL_TIME_UNIT = 50

var TIME_UNIT = INITIAL_TIME_UNIT // This is the only variable you must modify to alter the speed of the whole animation
export function setTIME_UNIT(v){
    TIME_UNIT = v    
}
export function getTIME_UNIT(){
    return TIME_UNIT
}

export var DURATION_LINK = 20 * TIME_UNIT

export var TICK = 10 * TIME_UNIT // 
var DURATION = TICK / 5 //10
//////////





// AFRAME.registerComponent('model-opacity', {
//     schema: { default: 1.0 },
//     init: function () {
//         this.el.addEventListener('model-loaded', this.update.bind(this));
//     },
//     update: function () {
//         var mesh = this.el.getObject3D('mesh');
//         var data = this.data;
//         if (!mesh) { return; }
//         mesh.traverse(function (node) {
//             if (node.isMesh) {
//                 node.material.opacity = data;
//                 node.material.transparent = data < 1.0;
//                 node.material.needsUpdate = true;
//             }
//         });
//     }
// });


function same(packet_new, packet_old) {
    return packet_new.hasOwnProperty("srcIface") && packet_old.hasOwnProperty("srcIface") &&
        // packet_old.srcIface.node != packet_new.srcIface.node &&
        JSON.stringify(packet_old.eth) === JSON.stringify(packet_new.eth)
}