import {
    MAX_TIME_UNIT,
	setTIME_UNIT, INITIAL_TIME_UNIT,
	getTIME_UNIT
} from "./index.js";
import { TT } from "./utils.js"


AFRAME.registerComponent('controller', {
	schema: {
		scale: { type: 'vec3' },
		PERIOD: { type: 'int', default: TT.toString() },
		position: { type: 'vec3' }
	},

	update: function (event) {
		let playPauseButton = document.querySelector("#playPauseButton");
		playPauseButton.textContent = '▶️';
		const slider = document.getElementById('slider');
		slider.value = INITIAL_TIME_UNIT
		setTIME_UNIT(INITIAL_TIME_UNIT)


		var networks = document.querySelectorAll('a-entity[network]:not([network=""])')
		networks.forEach((network) => {
			network.components["network"].set_latest_startTime(-2)
			network.components["network"].setCurrentTime(0)
		})

	},
	init: function () {
		let PERIOD = this.data.PERIOD


		var playPauseButton = document.querySelector("#playPauseButton");
		playPauseButton.textContent = '▶️';

		function event_listener_function(event) {
			var networks = document.querySelectorAll('a-entity[network]:not([network=""])')

			// toggle playPauseButton state icon
			var playPauseButton = document.querySelector("#playPauseButton");
			if (playPauseButton.textContent.trim() === '▶️') {
				// Cambiar a Pausa
				playPauseButton.textContent = '⏸️';
			} else {
				// Cambiar a Play
				playPauseButton.textContent = '▶️';
			}



			// var network = document.querySelector('#network')
			// var flying = network.components["network"].getFlying()


			networks.forEach((network) => {
				var flying = network.components["network"].getFlying()
				var network_id = network.id

				var playButton = document.querySelector("#playButton");

				switch (network.components["network"].getAnimationState()) {
					case 'INIT':

						network.components["network"].setNextPacket(0)

						network.components["network"].setCurrentTime(0)

						network.components["network"].setAnimationState("MOVING")

						setTimeout(() => { network.emit("next", { network: network.components["network"], startTime: -1 }, false) }, TT)

						break

					case 'MOVING':
						network.components["network"].setAnimationState("PAUSED")

						// playButton.setAttribute('color', 'gray')


						// Send to packets flying an animation-pause
						for (const packet of flying) {
							// pause animations of the packet and animations of the children
							packet.emit("animation-pause", null, false)
							for (const child of packet.children) {
								child.emit("animation-pause", null, false)
							}
						}

						break;

					case 'PAUSED':
						network.components["network"].setAnimationState("MOVING")

						//playButton.setAttribute('color', 'gray')

						// Send to packets flying an animation-resume

						for (const packet of flying) {
							// resume animations of the packet and animations of the children
							packet.emit("animation-resume", null, false)
							for (const child of packet.children) {
								child.emit("animation-resume", null, false)
							}
						}

						break
				} // switch

				network.addEventListener('next', network.components["network"].do_animate)

			}) //forEach

		} // event_listener_function



		var el = this.el
		function reset() {
			el.components["controller"].update()

			var networks = document.querySelectorAll('a-entity[network]:not([network=""])')

			// first we start those networks that do not depend on others
			networks.forEach((network) => {
				if (network.depends_on == "") {
					network.components["network"].update()
					network.components["network"].setAnimationState("INIT")
				}

			})
			// then we start those networks that do depend on others
			networks.forEach((network) => {
				if (network.depends_on != "") {
					network.components["network"].update()
					network.components["network"].setAnimationState("INIT")
				}

			})

		}




		// infoPanel
		let infoPanel = document.createElement('a-entity');
		infoPanel.setAttribute('html', 'cursor:#cursor;html:#info-panel');
		let position = Object.assign({}, this.data.position)

		position.x = -20
		position.y = 10.5
		position.z = 0
		infoPanel.setAttribute('position', position);
		infoPanel.setAttribute('scale', '40 40 40');
		infoPanel.setAttribute('id', 'infoPanel');

		infoPanel.setAttribute('look-at', "[camera]");

		this.el.sceneEl.appendChild(infoPanel);


		// prueba gui my-interface
		let guiPanel = document.createElement('a-entity');
		guiPanel.setAttribute('html', 'cursor:#cursor;html:#gui-panel');
		guiPanel.setAttribute('class', 'clickable');
		guiPanel.setAttribute('event-set__mouseenter', "scale: 1.5 1.5 1.5")
		guiPanel.setAttribute('event-set__mouseleave', "scale: 1 1 1")
		guiPanel.setAttribute('event-set__click', "material.color: blue")


		position = Object.assign({}, this.data.position)
		position.x += 0.7
		position.y += 0.6
		position.z += 0.5


		guiPanel.setAttribute('position', position);
		guiPanel.setAttribute('scale', '10 10 10');
		guiPanel.setAttribute('id', 'guiPanel');
		guiPanel.setAttribute('visible', true)
		this.el.appendChild(guiPanel);

		// playPause
		var playPauseButton = document.querySelector("#playPauseButton");
		guiPanel.setAttribute('sound', { on: 'click', src: '#playPause', volume: 5 });
		playPauseButton.addEventListener('click', event_listener_function)


		// stopButton
		var stopButton = document.querySelector("#stopButton");
		stopButton.addEventListener('click', reset)

		// settingsButton
		var settngsButton = document.querySelector("#settingsButton");
		settingsButton.addEventListener('click', settings)


		// settings panel activated by settingsButton
		var el = this.el
		var that = this
		function settings() {

			let fieldset = createSettingsFieldset(el)

			let settingsPanel = document.createElement('a-entity');
			settingsPanel.setAttribute('id', 'settingsPanel');
			settingsPanel.appendChild(fieldset)

			settingsPanel.setAttribute('html', 'cursor:#cursor;html:#settings-panel');
			settingsPanel.setAttribute('sound', { on: 'click', src: '#playPause', volume: 5 });
			settingsPanel.setAttribute('class', 'clickable');
			settingsPanel.setAttribute('event-set__mouseenter', "scale: 1.5 1.5 1.5")
			settingsPanel.setAttribute('event-set__mouseleave', "scale: 1 1 1")
			settingsPanel.setAttribute('event-set__click', "material.color: blue")
	
			position = Object.assign({}, that.data.position)
			position.x += 1.2
			position.y += 1.1
			position.z += 0.5
	
			settingsPanel.setAttribute('position', position);
			settingsPanel.setAttribute('scale', '10 10 10');
			settingsPanel.setAttribute('visible', true)


			el.appendChild(settingsPanel);
		}

		// slider speedSelector
		const slider = document.getElementById('slider');
		const sliderValue = document.getElementById('sliderValue');

		slider.addEventListener('input', () => {
			setTIME_UNIT(MAX_TIME_UNIT - Number(slider.value))
		});
	}
});

// Creates and returns a new fieldset for settings
function createSettingsFieldset(el) {
	const fieldset = document.createElement('field-set');
	fieldset.setAttribute('id', 'settings-panel')

	fieldset.style.display = 'inline-block';
    fieldset.style.background = 'rgb(173, 169, 169)';
    fieldset.style.color = '#333333';
    fieldset.style.borderRadius = '1em';
    fieldset.style.padding = '0.5rem';
    fieldset.style.margin = '0';
    fieldset.style.accentColor = 'rgb(15, 106, 186)';
    fieldset.style.width = '400px'; // Set the width
    fieldset.style.height = '200px'; // Set the height

	// Create legend
	const legend = document.createElement('legend');
	legend.textContent = 'Choose an option:';
	fieldset.appendChild(legend);

	// Define radio button options
	const options = [
		{ value: 'all', label: 'All', checked: true }, // Preset this option
		{ value: 'nothing', label: 'Nothing', checked: false }
	];

	// Create and append each radio button with its label
	options.forEach(option => {
		// Create the radio button
		const radio = document.createElement('input');
		radio.type = 'radio';
		radio.id = `radio-${option.value}`;
		radio.name = 'options'; // Group name for radio buttons
		radio.value = option.value;

		// Preset the radio button if `checked` is true
		if (option.checked) {
			radio.checked = true;
		}

		// Create the label for the radio button
		const label = document.createElement('label');
		label.textContent = option.label;
		label.setAttribute('for', radio.id);

		// Append radio button and label to fieldset
		fieldset.appendChild(radio);
		fieldset.appendChild(label);

		// Add a line break for better layout
		fieldset.appendChild(document.createElement('br'));
	});



	// Create and append the close button
	const closeButton = document.createElement('button');

	closeButton.textContent = 'Close';
	closeButton.className = 'close-btn';

	// Add click event to destroy the panel
	closeButton.addEventListener('click', (event) => {

		const settingsPanel = document.getElementById("settingsPanel");	
		console.log(settingsPanel)
		el.removeChild(settingsPanel)

	});

	fieldset.appendChild(closeButton);


	return fieldset;
}