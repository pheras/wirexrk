# **HTTP Demo**

The Wireshark preferences for JSON generation should be limited to the following:
   * Show TCP summary in protocol tree
   * Calculate conversation timestamps
   * Ignore TCP Timestamps in summary
   * Fast Retransmission supersede Out-of-Order interpretation
   * Do not call subdissectors for error packets
   * TCP Experimental Options using the format of RFC 6994

