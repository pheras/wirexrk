# **WireXRk**

WireXRk is a free/libre XR toolkit built with A-Frame. We originally conceived it to teach TCP/IP protocols. WireXRk merges Ethernet frames from each broadcast domain of a real or emulated network into a single PCAP file, which it uses to generate 3D interactive animations viewable in VR/AR goggles or in desktops.


To see a demo video, click the screenshot =>

[![WireXR Demo Video](https://gitlab.eif.urjc.es/pheras/vrnetvis/-/raw/main/doc/video/vxlan-2-layers.png)](https://youtu.be/0u0nv6SkEww)


# ***Teaching experiencies***

During the academic years 22/23 and 23/24 we have used 3D animations in the following courses of the Escuela de Ingeniería de Fuenlabrada (Fuenlabrada Engineering School) of the Universidad Rey Juan Carlos (URJC):
   * Computer Network Architecture (Degree in Telecommunications Technologies Engineering)
   * Internet Architecture (Degree in Audiovisual and Multimedia Systems Engineering)
   * Computer Networks (Degree in Biomedical Engineering)
   * Fundamentals of Computer Networks (Degree in Robotics Software Engineering)

   <img src="https://gitlab.eif.urjc.es/pheras/wirexrk/-/raw/main/doc/photos/VR-teaching-experiences.jpg" width="400" />

# ***Start VXLAN demo***

* Clone the repository:

    ```git clone https://gitlab.eif.urjc.es/pheras/wirexrk```

* Enter the wirexrk directory:

    ```cd wirexrk```

* Start HTTP server listening at port 9000 for instance: 

    ```python3 -m http.server 9000```

* Load the URL in Firefox: ```http://localhost:9000/vxlan```

* Press red play button to start/pause animation

* Press reset button to reload the animation from the beggining

# ***Scenarios***

* ping (./ping-desktop)

   <img src="https://gitlab.eif.urjc.es/pheras/wirexrk/-/raw/main/doc/images/ping.png" width="400" />

* traceroute (./traceroute-desktop)

   <img src="https://gitlab.eif.urjc.es/pheras/wirexrk/-/raw/main/doc/images/traceroute.png" width="400" />

* tcp (./tcp-desktop)

   <img src="https://gitlab.eif.urjc.es/pheras/wirexrk/-/raw/main/doc/images/tcp.png" width="400" />

* dns (./dns-desktop)

   <img src="https://gitlab.eif.urjc.es/pheras/wirexrk/-/raw/main/doc/images/dns.png" width="400" />

* switches (./switches)

   <img src="https://gitlab.eif.urjc.es/pheras/wirexrk/-/raw/main/doc/images/switches.png" width="400" />

* VXLAN (./vxlan)

   <img src="https://gitlab.eif.urjc.es/pheras/wirexrk/-/raw/main/doc/images/vxlan.png" width="400" />





